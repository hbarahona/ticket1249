class TicketsController < ApplicationController
	before_action :set_ticket, only: [:show, :edit, :update, :destroy]
	before_action :support_name, only: [:customer, :admin, :show]

	# GET /tickets
	def index
		redirect_according_account
	end

	def support
		@tickets = Ticket.where(["support = ? OR status = ?",current_user.id, "En Espera"])
	end

	def customer
		@tickets = current_user.tickets
	end

	def admin
		@tickets = Ticket.all
	end

	# GET /tickets/1
	def show
		@customer = User.select("name").where(id: @ticket.user_id).take
	end

	# GET /tickets/new
	def new
		@ticket = Ticket.new
		if is_support || is_admin
			@customers = User.select(:id, :name, :email).where(account: "C")
		else
			@ticket.user_id = current_user
		end
	end

	# GET /tickets/1/edit
	def edit
	end

	# POST /tickets
	def create
		@ticket = Ticket.new(ticket_params)
		@ticket.user_id = current_user.id
		if @ticket.save
			flash[:success] = t('.flashcreated')
			#UserMailer.ticket_ingresado(@ticket).deliver_now
			redirect_to @ticket
		else
			
			flash[:danger] = "error"
		 	render :new 
		end
	end

	# PATCH/PUT /tickets/1
	def update
		if @ticket.update(ticket_params)
			flash[:success] = t('.flashupdated')
			redirect_to @ticket 
		else
			render :edit
		end
	end

	# DELETE /tickets/1
	def destroy
		@ticket.destroy
		flash[:success] = t('.flashdeleted')
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_ticket
			@ticket = Ticket.find(params[:id])
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def ticket_params
			params.require(:ticket).permit(:title, :description, :status)
		end

		def support_name
			@support = User.select(:id, :name).where(account: "S")
		end
end
