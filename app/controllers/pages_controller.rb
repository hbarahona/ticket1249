class PagesController < ApplicationController
  def home
    if logged_in?
      redirect_according_account
    end
  end

  def contact
  end

  def support
  end

  def adminpanel
  end

  def customer
  end
end
