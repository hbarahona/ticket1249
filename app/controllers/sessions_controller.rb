class SessionsController < ApplicationController
	def new
		if logged_in?
			redirect_according_account
		end
	end

	def create
		@user = User.find_by(email: params[:session][:email].downcase)
		if @user && @user.authenticate(params[:session][:password])
			session[:user_id] = @user.id
			update_last_login
			redirect_according_account
		else
			flash[:danger] = t('.flasherrorlogin')
			render 'new'
		end
	end

	def destroy
		session[:user_id] = nil
		flash[:success] = t('.flashlogout')
		redirect_to root_url
	end
end