class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :current_user, :logged_in?, :redirect_according_account, :is_admin, :is_support, :is_customer, :update_last_login

  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options
    { locale: I18n.locale }
  end


  def current_user
  	@current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def logged_in?
  	!!current_user
  end

  def redirect_according_account
      if is_admin
        redirect_to adminticket_path
      elsif is_support
        redirect_to soporte_path
      elsif is_customer
        redirect_to cliente_path
      else
        redirect_to contacto_path        
      end
  end

  def is_admin
    current_user.account == 'A'
  end

  def is_support
    current_user.account == 'S'
  end

  def is_customer
    current_user.account == 'C'
  end

  def update_last_login
    current_user.lastlogin = Time.now
    current_user.save
  end

end
