class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :require_same_user, only: [:edit, :update]

  # GET /users
  # GET /users.json
  def index
    #@users = User.paginate(page: params[:pagge], per_page: 10)
    @users = User.where(account: ["C", "S"])
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      if logged_in?
        @user.createdby = current_user.id
        @user.save
      end
      #UserMailer.account_activation(@user).deliver_now
      flash[:success] = "La cuenta a sido creada exitosamente, se ha enviado un correo electrico a "+@user.email+" para activar su cuenta."
      redirect_to root 
    else
      flash[:danger] = "La cuenta no ha podido ser creada."
      render 'new'
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash.now[:success] = "Los datos han sido actualizados."
      redirect_according_account
    else
      render 'edit'
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    flash.now[:success] =  'User was successfully destroyed.'
    redirect_according_account
  end

  def activate
    @user.active = true
    @user.save 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :company, :address, :phone, :email, :password, :password_digest)
    end

    def require_same_user
      if current_user != @user
        flash[:danger] = "Error."
        redirect_to root_path
      end
      
    end
end
