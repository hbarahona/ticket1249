class RepliesController < ApplicationController

	def create
		@reply = current_user.replies.build(reply_params)
		@reply.ticket_id  = params[:ticket_id]
		
		if @reply.save
			#UserMailer.nueva_respuesta(@reply.ticket).deliver_now
			flash[:success] = "La respuesta ha sido ingresada."
		else
			flash[:danger] = "Error al ingresar el nuevo comentario"
		end
		redirect_to ticket_path(params[:ticket_id])
	end




	private
		def reply_params
				params.require(:reply).permit(:description, :id_user, :id_ticket)
		end
end