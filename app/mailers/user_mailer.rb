class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.account_activation.subject
  #
  def ticket_ingresado(ticket)
    @ticket = ticket
    mail to: "noreply@ticket.demo.com", bcc: @ticket.user.email, subject: "Ticket #{@ticket.id} ingresado al sistema."
  end

  def nueva_respuesta(ticket)
    @ticket = ticket
    mail to: "noreply@ticket.demo.com", bcc: @ticket.user.email, subject: "Se ha respondido la solicitud #{@ticket.id} RE: #{@ticket.title} ."
  end

  def account_activation(user)
    @user = user
    mail to: @user.email, subject: "Activacion de cuenta."
  end

  def password_reset(user)
    @user = user
    @greeting = "Hi"
    mail to: "to@example.org"
  end



end
