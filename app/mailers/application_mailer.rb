class ApplicationMailer < ActionMailer::Base
  default from: "noreply@ticket.demo.com"
  layout 'mailer'
end
