class Reply < ActiveRecord::Base
	belongs_to :ticket
	belongs_to :user

	validates :user_id, presence: true
	validates :ticket_id, presence: true
	validates :description, presence: true, length: { minimum: 3, maximum: 500 }
end
