class User < ActiveRecord::Base
	attr_accessor :activation_token

	has_many :tickets
	has_many :replies

	#name
	before_save { self.name = name.titleize }
	validates :name, presence: true, length: { minimum: 3, maximum: 50 }
	
	#email
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
	before_save { self.email = email.downcase }
	validates :email, presence: true, length: { minimum: 5, maximum: 100 }, uniqueness: { case_sensitive: false }, format: { with: VALID_EMAIL_REGEX }
	
	#password
	has_secure_password
	#validates :password, length: { minimum: 6 }

	# account and token
	before_create :create_activation_digest

	def User.new_token
		SecureRandom.urlsafe_base64
	end

	def self.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end


	private
	
		def create_activation_digest
			self.activation_token = User.new_token 
			self.activation_digest = BCrypt::Password.create(activation_token)
			#self.activation_digest = User.digest(activation_token)
			
		end


end