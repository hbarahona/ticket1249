class Ticket < ActiveRecord::Base
	belongs_to :user
	has_many :replies

	validates :user_id, presence: true
	validates :title, presence: true, length: { minimum: 3, maximum: 35 }
	validates :description, presence: true, length: { minimum: 3, maximum: 500 }
end

