Rails.application.routes.draw do

  scope "(:locale)", locale: /en|es/ do
    root 'sessions#new'

    get 'registro', to: 'users#new'
    get 'usuarios', to: 'users#index'
    
    get 'login', to: 'sessions#new'
    post 'login', to: 'sessions#create'
    get 'logout', to: 'sessions#destroy'

    get 'soporte', to: 'tickets#support'
    get 'cliente', to: 'tickets#customer'
    get 'adminticket',  to: 'tickets#admin'
    get 'tickets', to: 'tickets#index'
    get 'ingresoticket', to: 'tickets#new'
    
    get 'administracion', to: 'pages#adminpanel'
    get 'contacto', to: 'pages#contact'
    get 'soporte', to: 'pages#support'

    resources :tickets
    resources :users
    resources :tickets do
      resources :replies
    end

    resources :account_activations, only: [:edit]
  end
end