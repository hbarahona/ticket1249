require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get support" do
    get :support
    assert_response :success
  end

  test "should get adminpanel" do
    get :adminpanel
    assert_response :success
  end

  test "should get customer" do
    get :customer
    assert_response :success
  end

end
