class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :company
      t.string :address
      t.string :address2
      t.string :phone
      t.string :phone2
      t.string :email
      t.string :password_digest
      t.string :account
      t.datetime :lastlogin
      t.integer :createdby
      t.string  :activation_digest
      t.boolean  :activated, default: false
      t.datetime  :activated_at

      t.timestamps null: false
    end
  end
end
