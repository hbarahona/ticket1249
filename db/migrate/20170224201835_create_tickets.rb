class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :user_id
      t.integer :support
      t.string :priority, default: "Normal"
      t.string :status, default: "En Espera"
      t.string :title
      t.string :description

      t.timestamps null: false
    end
  end
end
