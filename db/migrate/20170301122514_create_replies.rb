class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.string :description
      t.references :ticket, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
